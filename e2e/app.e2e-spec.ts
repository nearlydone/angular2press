import { Angular2pressPage } from './app.po'

describe('angular2press App', () => {
	let page: Angular2pressPage

	beforeEach(() => {
		page = new Angular2pressPage()
	})

	it('should display message saying app works', () => {
		page.navigateTo()
		expect(page.getParagraphText()).toEqual('app works!')
	})
})
