FROM nginx

ADD conf/production/nginx /etc/nginx
ADD dist /usr/share/nginx/html
