
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class APIService {
	private core: string = '/api/'

	constructor(private http: HttpClient) { }

	retrieve(path: String) {
		return this.http
			.get(this.core + path)
	}

}
