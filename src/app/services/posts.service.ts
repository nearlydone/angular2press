import { Injectable } from '@angular/core'
import { APIService } from './api.service'

@Injectable()
export class PostsService {

	private path: string = 'posts/'

	constructor(private api: APIService) { }

	getPosts() {
		return this.api.retrieve(this.path)
	}

	getPost(id: any) {
		return this.api.retrieve(this.path + id)
	}

}
