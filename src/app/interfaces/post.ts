export interface Post {
	id: Number,
	content: {
		rendered: String
	},
	title: {
		rendered: String
	}
}
