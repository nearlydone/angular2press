import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component'
import { APIService } from './services/api.service'
import { PostsService } from './services/posts.service'
import { PostListComponent } from './components/posts/post-list/post-list.component'

const routeDefinitions: Routes = [{
	path: 'posts',
	component: PostListComponent
// }, {
// 	path: 'posts/:postID',
// 	component: PostComponent
// }, {
// 	path: 'pages',
// 	component: PagesComponent
// }, {
// 	path: 'pages/:pageID',
// 	component: PageComponent
}]

@NgModule({
	declarations: [
		AppComponent,
		PostListComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpClientModule,
		RouterModule.forRoot(routeDefinitions, {
			useHash: true
		})
	],
	providers: [APIService, PostsService],
	bootstrap: [AppComponent]
})
export class AppModule { }
