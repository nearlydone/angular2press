import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { APIService } from '../../../services/api.service'
import { PostsService } from '../../../services/posts.service'
import { Post } from '../../../interfaces/post'

@Component({
	selector: 'app-post-list',
	templateUrl: './post-list.component.html',
	styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

	public posts: Array<Post> = []

	constructor(private postService: PostsService, private router: Router, private route: ActivatedRoute) { }

	getPosts() {
		this.postService
			.getPosts()
			.subscribe((a: Array<Post>) => {
				this.posts = a
			})
	}

	ngOnInit() {
		this.getPosts()
	}

	selectPost(postID: Number) {
		this.router.navigate([ postID ], {
			relativeTo: this.route
		})
	}

}
