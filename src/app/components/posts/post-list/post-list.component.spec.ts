import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { APIService } from '../../../services/api.service'
import { PostsService } from '../../../services/posts.service'
import { PostListComponent } from './post-list.component'

describe('PostListComponent', () => {
	let component: PostListComponent
	let fixture: ComponentFixture<PostListComponent>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PostListComponent],
			providers: [APIService, PostsService]
		})
		.compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(PostListComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should be created', () => {
		expect(component).toBeTruthy()
	})
})
